package ru.tinkoff.studentclub.dao.db

import java.util.UUID

import ru.tinkoff.studentclub.dao.OrderDao
import ru.tinkoff.studentclub.domain.Order
import slick.dbio.Effect.Read

import scala.concurrent.Future
import concurrent.ExecutionContext.Implicits.global

class SlickOrderDao extends OrderDao {
  import Schema._
  import Schema.profile.api._

  override def sumByClients(): Future[Map[UUID, BigDecimal]] = {
    db.run {
      val orderStats: IO[Seq[(UUID, Option[BigDecimal])], Read] =
        orders
          .groupBy(_.clientId)
          .map { case (id, query) => (id, query.map(_.price).sum) }
          .result

      orderStats
        .map(_.foldLeft(Map[UUID, BigDecimal]()){ case (acc, (id, priceOpt)) =>
            val newTotal = acc.getOrElse(id, BigDecimal(0)) + priceOpt.getOrElse(0)
            acc.updated(id, newTotal)
        })
    }
  }

  override def insert(order: Order): Future[Unit] = Future.unit // TODO

  override def find(id: UUID): Future[Option[Order]] = Future.successful(None) //TODO

  override def list(clientId: UUID): Future[Seq[Order]] = Future.successful(Seq()) // TODO

  // use table.filter(...).map(_.fieldToUpdate).update(newValue)
  override def assign(orderId: UUID, executorId: UUID): Future[Unit] = Future.unit //TODO
}
