package ru.tinkoff.studentclub.dao.db

import ru.tinkoff.studentclub.dao.OneClickOrderDao
import ru.tinkoff.studentclub.domain.{Client, Order}

import scala.concurrent.Future

class SlickOneClickOrderDao extends OneClickOrderDao {

  // Use dbAction.andThan(nextDbAction).transactionally
  override def insertAll(client: Client, order: Order): Future[Unit] = Future.unit //TODO
}
