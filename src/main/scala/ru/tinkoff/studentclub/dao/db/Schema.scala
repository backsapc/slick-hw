package ru.tinkoff.studentclub.dao.db

import ru.tinkoff.studentclub.domain.{Order, Executor}
import slick.jdbc.JdbcProfile

// Use H2Profile to connect to an H2 database
import java.util.UUID


import scala.concurrent.Future
import ru.tinkoff.studentclub.domain.Client
import slick.lifted.ProvenShape

object Schema {

  import slick.jdbc.H2Profile.api._

  implicit val uuidMapper: BaseColumnType[String] = MappedColumnType.base[String, UUID](UUID.fromString, _.toString)

  class Clients(tag: Tag) extends Table[Client](tag, "CLIENTS") {
    def id: Rep[UUID] = column("ID", O.PrimaryKey)

    def name: Rep[String] = column("NAME")

    def phone: Rep[String] = column("PHONE")

    override def * : ProvenShape[Client] = (id, name, phone) <> (Client.tupled, Client.unapply)
  }

  val clients = TableQuery[Clients]

  class Executors(tag: Tag) extends Table[Executor](tag, "EXECUTORS") {
    def id: Rep[UUID] = column("ID", O.PrimaryKey)

    def name: Rep[String] = column("NAME")

    def phone: Rep[String] = column("PHONE")

    override def * = (id, name, phone) <> (Executor.tupled, Executor.unapply)
  }

  val executors = TableQuery[Executors]

  class Orders(tag: Tag) extends Table[Order](tag, "ORDERS") {
    def id: Rep[UUID] = column("ID")

    def description: Rep[String] = column("DESCRIPTION")

    def clientId: Rep[UUID] = column("CLIENT_ID")

    def price: Rep[BigDecimal] = column("PRICE")

    def executorId: Rep[Option[UUID]] = column("EXECUTOR_ID")

    override def * = (id, description, price, clientId, executorId) <> (Order.tupled, Order.unapply)
  }

  val orders = TableQuery[Orders]

  def setup(): Future[Unit] = db.run((
    Schema.clients.schema ++ Schema.executors.schema ++ Schema.orders.schema
    ).create)

  val profile: JdbcProfile = slick.jdbc.H2Profile

  val db = Database.forConfig("h2mem1")

  type IO[+R, -E <: Effect] = DBIOAction[R, NoStream, E]
}