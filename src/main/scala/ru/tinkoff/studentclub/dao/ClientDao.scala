package ru.tinkoff.studentclub.dao

import java.util.UUID

import ru.tinkoff.studentclub.domain.Client

import scala.concurrent.Future

trait ClientDao {
  def insert(client: Client): Future[Unit]

  def exists(clientId: UUID): Future[Boolean]
}
