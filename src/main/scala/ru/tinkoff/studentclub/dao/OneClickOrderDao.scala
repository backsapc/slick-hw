package ru.tinkoff.studentclub.dao

import ru.tinkoff.studentclub.domain.{Client, Order}

import scala.concurrent.Future

trait OneClickOrderDao {
  def insertAll(client: Client,
                order: Order): Future[Unit] // creates client and order
}
