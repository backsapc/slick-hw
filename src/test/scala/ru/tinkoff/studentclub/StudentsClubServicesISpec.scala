package ru.tinkoff.studentclub

import org.scalatest.concurrent.Futures
import org.scalatest.{BeforeAndAfterAll, Matchers, AsyncFlatSpec}
import ru.tinkoff.studentclub.dao.db.Schema

import scala.concurrent.Await
import scala.concurrent.duration.Duration

abstract class StudentsClubServicesISpec extends AsyncFlatSpec with StudentsClubServices with Matchers with Futures {
  "client registration service" should "register client" in {
    val clientPhone = "+1000"
    val clientName = "lol"
    clientRegistrationService.register(clientName, clientPhone)
      .map { client =>
        client.name shouldBe clientName
        client.phone shouldBe clientPhone
      }
  }

  //TODO executor registration test

  "order service" should "return statistics by client" in { // TODO fix it
    for {
      lol <- clientRegistrationService.register("lol", "+999")
      kek <- executorRegistrationService.register("kek", "+777")
      order <- orderService.create("Борщ", 100, lol.id)
      _ <- orderService.assign(order.id, kek.id)
      stats <- orderService.sumByClients()
    } yield {
      stats shouldBe Map(lol.id -> BigDecimal(100))
    }
  }

  // TODO one click orders test
}

class ImMemoryStudentsClubServicesISpec extends StudentsClubServicesISpec with InMemoryStudentsClub

class DbStudentsClubServicesISpec extends StudentsClubServicesISpec with SlickStudentsClub with BeforeAndAfterAll with Futures {
  override protected def beforeAll(): Unit = {
    Await.result(Schema.setup(), Duration.Inf)
  }
}
